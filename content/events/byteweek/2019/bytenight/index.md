---
startdate:  2019-02-02
starttime: "18:00"
linktitle: "Bytenight"
title: "Bytenight 2019"
location: "HSBXL"
eventtype: "Thé dansant"
price: ""
image: "bytenight.png"
series: "byteweek2019"
aliases: [/bytenight/]
---


As an after-party to the big FOSDEM Free Software conference in Brussels,  
the oldest Hackerspace in town organises its annual free party.

# 10^3

- 10 years HSBXL
- 10 years .be hackerspaces
- 10 years Bytenight
- (5th location)
- (500th Techtuesday)

![Party!](party.jpg "Party!")


# Music
Weird music for weird people.

- CIRC8
- [Jonny and the Bomb](https://soundcloud.com/4a-6f-6e-6e-79-20-2b-20-5)
- [3D63](https://soundcloud.com/3d63)
- [planète concrète](http://planeteconcrete.tumblr.com/) pesence/interface
- [this is my condition](https://web.archive.org/web/20130425081518/http://www.thisismycondition.com/)

# Unsupported, deprecated versions
- [Bytenight v2018](https://wiki.hsbxl.be/Bytenight_2018)
- [Bytenight v2017](https://wiki.hsbxl.be/Bytenight_2017)
- [Bytenight v2016](https://wiki.hsbxl.be/Bytenight_(2016))
- [Bytenight v2015](https://wiki.hsbxl.be/Bytenight_(2015))
- [Bytenight v2014](https://wiki.hsbxl.be/Bytenight_(2014))
- [Bytenight v2013](https://wiki.hsbxl.be/Bytenight_2013)
- [Bytenight v2012](https://wiki.hsbxl.be/ByteNight_(2012))
- [Bytenight v2011](https://wiki.hsbxl.be/ByteNight_(2011))
- [Bytenight v2010](https://wiki.hsbxl.be/ByteNight_(2010))


# Organizing
Notes can be found on https://etherpad.openstack.org/p/bytenight2019

